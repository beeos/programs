import { Pipe, IStdIO, KernelCall, IEstablishStdConnection, IProcess } from "@beeos/program"

const pipes: IStdIO = {
  in: [],
  out: []
}

let pid = 0;

let stdIOConnection = false;

function registerOutPipe(pipe: Pipe) {
  pipes.out.push(pipe);
}

function sendMessageToStdOut(message: string) {
  pipes.out.forEach((pipe: Pipe) => pipe(message));
}

function main(call: KernelCall, connection?: IEstablishStdConnection, args?: string[]) {
  if (connection) {
    stdIOConnection = true;
    registerOutPipe(connection.establishStdOut());
  }
  return new Promise<IProcess>((resolve, reject) => {
    if (args) {
      const contents = call('fs-read-dir', args[1] ? args[1] : args[0]);
      if (stdIOConnection) {
        sendMessageToStdOut(contents.join("\n") + "\n");
      }
      resolve({
        exitCode: 0
      });
    }
  });
}

execute(main);
