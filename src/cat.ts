import { Pipe, IStdIO, KernelCall, IEstablishStdConnection } from '@beeos/program';

const pipes: IStdIO = {
  in: [],
  out: []
};

function registerOutPipe(pipe: Pipe) {
  pipes.out.push(pipe);
}

function sendMessageToStdOut(message: string) {
  pipes.out.forEach(pipe => pipe(message));
}

async function main(call: KernelCall, connection: IEstablishStdConnection, args: string[]) {
  if (connection) {
    registerOutPipe(connection.establishStdOut());
    if (args) {
      const file = call('fs-read-file', args[0] + "/" + args[1]).toString();
      sendMessageToStdOut(file + "\n");
    }
  }
  return {exitCode: 0};
}

execute(main);
