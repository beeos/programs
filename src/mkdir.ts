import { KernelCall, IEstablishStdConnection } from '@beeos/program';

async function main(call: KernelCall, connection: IEstablishStdConnection, args: string[]) {
  const path = args[1][0] === '/' ? args[1] : args[0] + '/' + args[1];
  const out = connection.establishStdOut();
  if (!call('fs-exists', path)) {
    try {
      call('fs-make-directory', path);
      out(`Created directory ${path}`);
    }
    catch (err) {
      return {exitCode: 1};
    }
  }
  else out(`Directory ${path} already exists.`);
  return {exitCode: 0};
}
