import { KernelCall, IEstablishStdConnection } from "@beeos/program";

async function main(call: KernelCall, connection: IEstablishStdConnection, args: string[]) {
  const out = connection.establishStdOut();
  out(args.join(" "));
  return {exitCode: 0}
}

execute(main);
