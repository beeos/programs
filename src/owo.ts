import { IProcess, KernelCall, IEstablishStdConnection } from "@beeos/program";

declare var execute: any;

const cat = `                _                        
\\\`*-.                    
 )  _\\\`-.                 
.  : \\\`. .                
: _   '  \\               
; *\\\` _.   \\\`*-._          
\\\`-.-'          \\\`-.       
  ;       \\\`       \\\`.     
  :.       .        \\    
  . \\  .   :   .-'   .   
  '  \\\`+.;  ;  '      :   
  :  '  |    ;       ;-. 
  ; '   : :\\\`-:     _.\\\`* ;
      .*' /  .*' ; .*\\\`- +'  \\\`*' 
\\\`*-*   \\\`*-*  \\\`*-*'        `;

async function main(call: KernelCall, stdio: IEstablishStdConnection): Promise<IProcess> {
  stdio.establishStdOut()(cat);
  return {
    exitCode: 0
  }
}

execute(main);
