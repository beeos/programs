const gulp = require('gulp');
const ts = require('gulp-typescript');

gulp.task('tsc', () => {
  const proj = ts.createProject('./tsconfig.json');
  gulp.src('./src/**/*.ts')
    .pipe(proj()).js
    .pipe(gulp.dest('./bin'))
});

gulp.task('default', ['tsc']);

